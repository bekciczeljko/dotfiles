function pandoc(target)
    execute('! pandoc ' . expand('%:p') . ' -o ' expand('%:r') . target)
endfunction

nnoremap <leader>p :Pandoc pdf
