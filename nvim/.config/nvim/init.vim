let g:plugin_path = $HOME . '/.local/share/nvim/plugged/'

set nocompatible
filetype off

call plug#begin(g:plugin_path)

Plug 'tpope/vim-fugitive'
Plug 'itchyny/lightline.vim'
Plug 'bling/vim-bufferline'
Plug 'vimlab/split-term.vim'
Plug 'mbbill/undotree'
Plug 'dhruvasagar/vim-table-mode'
Plug 'tpope/vim-surround'
Plug 'altercation/vim-colors-solarized'
Plug 'junegunn/goyo.vim'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'preservim/nerdtree'
Plug 'hashivim/vim-terraform'

call plug#end()
filetype plugin indent on

colorscheme solarized

syntax enable

set cursorline
set guicursor=

set encoding=UTF-8
set clipboard+=unnamedplus
set shell=/bin/bash
set completeopt=menuone,menu,longest
set hidden
set cmdheight=2
set noshowmode
set noerrorbells
set title
set splitbelow
set splitright

let mapleader = " "

nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

source ~/.config/nvim/settings/tab-settings.vim
source ~/.config/nvim/settings/search-settings.vim
source ~/.config/nvim/settings/line-numbering-text-width.vim
source ~/.config/nvim/settings/swap-backup-undodir-settings.vim
source ~/.config/nvim/settings/python-settings.vim
source ~/.config/nvim/settings/vim-table-mode-settings.vim
source ~/.config/nvim/settings/buffer-movement-settings.vim
source ~/.config/nvim/settings/goyo-settings.vim
source ~/.config/nvim/commands/notes-commands.vim

