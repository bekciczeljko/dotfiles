let g:pymode_python = 'python3'
let g:python3_venv = $HOME . '/.local/nvim/virtualenvs/neovim3/'
let g:python3_host_prog = g:python3_venv . '/bin/python3'

if !isdirectory(g:python3_venv) && executable('virtualenv')
    " the python docs for vim state that we only need pynvim because they
    " renamed neovim to pynvim
    execute( '! mkdir -p ' . g:python3_venv . '2> /dev/null;' .
           \ 'virtualenv --python /usr/bin/python3 ' . g:python3_venv . ' 2> /dev/null;' .
           \ 'source ' . g:python3_venv . '/bin/activate' . ' 2> /dev/null;' .
           \ 'pip install pynvim 2> /dev/null'
           \ )
endif
