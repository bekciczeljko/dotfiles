" #SWAPFILE #BACKUP #UNDODIR
" do not use swapfile or backup files instead use the undotree and setup it 
" automatically if it does not exists.

set noswapfile
set nobackup
set nowritebackup
set undofile

nnoremap <leader>u :UndotreeToggle<CR>
