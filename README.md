# DOTFILES

Hi! Here are my dotfiles and these are 'managed' with GNU Stow.

## How to install the things here?


If you want to use my zsh config for example then just `stow zsh` or `stow --target=$HOME zsh/`. The first command
assumes the dofiles are in `~/dotfiles`. The other one can do it from anywhere! Then you will have the contents of the
`zsh` directory symlinked to your home directory.

## What am I using?

Here is a list of some programs I use mostly with links to the project.

* [NeoVim](https://www.neovim.io) 
* [VimPlug](https://github.com/junegunn/vim-plug) 
* [zsh](https://www.zsh.org) 
* [antidote](https://getantidote.github.io/)
* [GNU Stow](https://www.gnu.org/software/stow/) 
* [Starship](starship.rs)
* [iTerm2](https://iterm2.com/)
* and more

