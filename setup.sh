configs=(
    ipython
    iterm2
    nvim
    scripts
    starship
    zsh
)

for c in ${configs[@]}
do
    echo ">>> $c"
    stow --target="$HOME" "$c"
done
