import importlib
import sys 

imports = [
    {'package': 'numpy', 'as': 'np'},
    {'package': 'pandas', 'as': 'pd'},
    {'package': 'matplotlib.pyplot', 'as': 'plt'}
]


    p = i['package']
    a = i['as']

    try:
        module_object = importlib.import_module(p)
        vars()[a] = module_object
        print("😀 - {} imported as {}".format(p, a))
    except ImportError:
        print("😑 - {} not installed".format(p))

